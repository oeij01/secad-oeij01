#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <unistd.h>

int main (int argc, char *argv[])
{



	if(argc!=3){
        	printf("Usage: %s <server> <port>\n", argv[0]);
        	exit(1);
    }
	printf("TCP Client Program by James Oei\n");
	char *servername = argv[1];
	char *port = argv[2];

	if(strlen(servername) > 255 || strlen(port) > 5){
        	perror("Severname or port is too long!\n");
        	exit(1);
    }
	printf("Servername= %s, port=%s\n",servername,port);
	int sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd < 0){
		perror("Error in openning a socket!\n");
		exit(2);
    }

	printf("A socket is openned\n");
	struct addrinfo hints, *serveraddr;
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	int dns_status = getaddrinfo(servername,port,&hints,&serveraddr);

	if(dns_status != 0){
		perror("Cannot get the server address!\n");
		exit(3);
    }
	int connected = connect(sockfd,serveraddr->ai_addr, serveraddr->ai_addrlen);
	if(connected<0){
        	perror("Error in connecting to the server!\n");
        	exit(4);
    }
	if(connected <0){
        	perror("Cannot connect to the server\n");
        	exit(4);
    }
	printf("Connected to the server %s at port %s\n", servername,port);
	freeaddrinfo(serveraddr);

	int BUFFERSIZE = 1024; //define the size of the buffer
	char buffer[BUFFERSIZE]; //define the buffer
	bzero(buffer,BUFFERSIZE); //empty the buffer
	printf("Enter your message to send:");
	fgets(buffer, BUFFERSIZE, stdin);
	int byte_sent = send(sockfd,buffer,strlen(buffer),0);
	bzero(buffer,BUFFERSIZE);
	int byte_received = recv(sockfd, buffer, BUFFERSIZE, 0);
	if(byte_received <0){
        	perror("Error in reading");
        	exit(4);
    }
	printf("Received from server: %s", buffer);
	close(sockfd); //need to include: #include <unistd.h>
	return 0;
}





