/* Simple EchoServer in GoLang by Phu Phung, customized by <YOUR NAME> for SecAD*/
package main

import (
	"fmt"
	"net"
	"os"
)

const BUFFERSIZE int = 1024
var allClient_conns = make(map[net.Conn]string)
var newclient = make(chan net.Conn)
var lostclient = make(chan net.Conn)

func main() {
	if len(os.Args) != 2 {
		fmt.Printf("Usage: %s <port>\n", os.Args[0])
		os.Exit(0)
	}
	port := os.Args[1]
	if len(port) > 5 {
		fmt.Println("Invalid port value. Try again!")
		os.Exit(1)
	}
	server, err := net.Listen("tcp", ":"+port)
	if err != nil {
		fmt.Printf("Cannot listen on port '" + port + "'!\n")
		os.Exit(2)
	}
	fmt.Println("EchoServer in GoLang developed by Phu Phung, SecAD, revised by James Oei")
	fmt.Printf("EchoServer is listening on port '%s' ...\n", port)
go func(){
	for{
	client_conn, _ := server.Accept()
	newclient <- client_conn
}
}()
for {
	select{
		case client_conn := <- newclient:
		allClient_conns[client_conn] = client_conn.RemoteAddr().String()
		welcomemessage := fmt.Sprintf("A new client '%s' connected!\n# of connected clients: %d\n",client_conn.RemoteAddr().String(),len(allClient_conns))
	fmt.Println(welcomemessage)
	sendtoAll([]byte (welcomemessage))
	go client_goroutine(client_conn)
	case client_conn := <- lostclient:
//handling for the event
	delete(allClient_conns, client_conn)
	byemessage := fmt.Sprintf("Client '%s' is DISCONNECTED!\n# of connected clients: %d\n",
	client_conn.RemoteAddr().String(), len(allClient_conns))
	sendtoAll([]byte(byemessage)) 
}
}
	 //44
	}


func client_goroutine(client_conn net.Conn){
/*key statements for a client will be
pasted here*/
	
	var buffer [BUFFERSIZE]byte
go func(){
	for {
		byte_received, read_err := client_conn.Read(buffer[0:])
		if read_err != nil {
			fmt.Println("Error in receiving...")
			lostclient <- client_conn
			return
		}
		/* _, write_err := client_conn.Write(buffer[0:byte_received])
		if write_err != nil {
			fmt.Println("Error in sending...")
			return
		}
		fmt.Printf("Received data: %sEchoed back!\n", buffer) */
		sendtoAll(buffer[0:byte_received])
}
}()
}
func sendtoAll(data []byte){
	for client_conn, _ := range allClient_conns{_, write_err :=client_conn.Write(data)
	if write_err != nil {
		fmt.Println("Error in sending...")
		continue
		}
	}
fmt.Printf("Send data: %s to all clients!\n", data)
}

