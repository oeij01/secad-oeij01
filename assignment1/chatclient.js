var net = require('net');
var readlineSync = require('readline-sync');
var username;
var password;

function loginsync(){
	username = readlineSync.question('Username:');
	if(!inputValidated(username)) {
		console.log("Username must have at least 5 characters. Please Try again!)");
		loginsync();
		return;
	}
	password = readlineSync.question('Password:', {
		hideEchoBack: true
	});
	if(!inputValidated(password)){
		console.log("Password must have at least 5 characters. Please Try again!");
		loginsync()
		return;
	}
	var login = `{"Username": "${username}", "Password": "${password}"}`;
	//client.write(login);
}

function inputValidated(text){
	return (text.length > 4)
}

if(process.argv.length != 4){
	console.log("Usage: node %s <host> <port>", process.argv[1]);
	process.exit(1);
}
var host=process.argv[2];
var port=process.argv[3];

if(host.length >253 || port.length >5 ){
	console.log("Invalid host or port. Try again!\nUsage: node %s <port>", 		process.argv[1]);
	process.exit(1);
}
var client = new net.Socket();
console.log("Simple telnet.js developed by James Oei, SecAD");
console.log("Connecting to: %s:%s", host, port);
client.connect(port,host, connected);

function connected(){
	console.log("Connected to: %s:%s", client.remoteAddress, client.remotePort);
	keyboard.pause();
	loginsync();
	keyboard.prompt();
}

client.on("data", data => {
console.log("Received data:" + data);

});
client.on("error", function(err){
console.log("Error");
process.exit(2);
});
client.on("close", function(data){
console.log("Connection has been disconnected");
process.exit(3);

});

const keyboard = require('readline').createInterface({
input: process.stdin,
output: process.stdout
});
keyboard.on('line', (input) => {
console.log(`You typed: ${input}`);
if(input == ".exit"){
	client.destroy();
	console.log("disconnected!");
	process.exit();
} else
	client.write(input);
});
