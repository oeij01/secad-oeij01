/* Simple EchoServer in GoLang by Phu Phung, customized by James Oei for SecAD*/
package main

import (
	"fmt"
	"net"
	"os"
	"encoding/json"
)

const BUFFERSIZE int = 1024
//var allClient_conns = make(map[net.Conn]string)
var AuthenticatedClient_conns = make(map[net.Conn]string)
var newclient = make(chan net.Conn)
var lostclient = make(chan net.Conn)
func main() {
	if len(os.Args) != 2 {
		fmt.Printf("Usage: %s <port>\n", os.Args[0])
		os.Exit(0)
	}
	port := os.Args[1]
	if len(port) > 5 {
		fmt.Println("Invalid port value. Try again!")
		os.Exit(1)
	}
	server, err := net.Listen("tcp", ":"+port)
	if err != nil {
		fmt.Printf("Cannot listen on port '" + port + "'!\n")
		os.Exit(2)
	}
	fmt.Println("EchoServer in GoLang developed by Phu Phung, SecAD, revised by James Oei")
	fmt.Printf("EchoServer is listening on port '%s' ...\n", port)
go func(){
	for{
	client_conn, _ := server.Accept()
	go login(client_conn)
	newclient <- client_conn
}
}()
for {
	select{
		case client_conn := <- newclient:
			go authenticating(client_conn)
			
		case client_conn := <- lostclient:
//handling for the event
		delete(AuthenticatedClient_conns,client_conn)
		byemessage := fmt.Sprintf("Client '%s' is DISCONNECTED!\n# of connected clients: %d\n",
	client_conn.RemoteAddr().String(), len(AuthenticatedClient_conns))
	go sendtoAll([]byte(byemessage)) 
}
}
	 //44
	}


func client_goroutine(client_conn net.Conn){
/*key statements for a client will be
pasted here*/
	
	var buffer [BUFFERSIZE]byte
go func(){
	for {
		byte_received, read_err := client_conn.Read(buffer[0:])
		if read_err != nil {
			fmt.Println("Error in receiving...")
			lostclient <- client_conn
			return
		}
		/*_, write_err := client_conn.Write(buffer[0:byte_received])
		if write_err != nil {
			fmt.Println("Error in sending...")
			return
		}
		fmt.Printf("Received data: %sEchoed back!\n", buffer) */
		sendtoAll(buffer[0:byte_received])
}
}()
}
func sendtoAll(data []byte){
	for client_conn, _ := range AuthenticatedClient_conns{
		_,write_err := client_conn.Write(data)
		if write_err != nil {
			fmt.Println("Error in sending...")
			continue
		}
	}
fmt.Printf("Send data: %s to all clients!\n", data)
}
func login(client_conn net.Conn){
	var buffer [BUFFERSIZE]byte
	go func(){
		for {
			byte_received, read_err := client_conn.Read(buffer[0:])
			if read_err != nil {
				fmt.Println("Error in receiving...")
				lostclient <- client_conn
				return
			}
			logindata := buffer[0:byte_received]
			fmt.Printf("Received data: %s, len=%d\n", logindata, len(logindata))
			authenticated, username, loginmessage := checklogin(logindata)

		//f len(clientdata) >= 5 && strings.Compare(string(clientdata[0:5]),"login") == 0 {
				if authenticated{
				fmt.Println("Debug -> login data. User '" + username + "' is successfully logged in")
				sendto(client_conn, []byte("login data\n"))

			}else{
				fmt.Println("Debug -> non-login data. Erro message: " + loginmessage)
				sendto(client_conn,[]byte(loginmessage))
				login(client_conn)
		}
	}
	}()
	} 

func sendto(client_conn net.Conn, data [] byte) {
	_, write_err := client_conn.Write(data)
	if write_err != nil {
		fmt.Println("Error in sending....to " + client_conn.RemoteAddr().String())
		return
	}
}

func checklogin(data []byte) (bool, string, string){
	type Account struct{
		Username string
		Password string
	}
	fmt.Printf("%s",data)
	var account Account
	err := json.Unmarshal(data, &account)
	if err!= nil || account.Username == "" || account.Password == "" {
		fmt.Printf("JSON parsing error: %s\n", err)
		return false, "", `[BAD LOGIN] Expected: {"Username": "...", "Password": "..."}`

	}
	fmt.Printf("DEBUG>Got : account= %s\n", account)
	fmt.Printf("DEBUG>Got: username = %s, password = %s\n", account.Username, account.Password)

	if checkaccount(account.Username,account.Password) {
		return true, account.Username, "logged"
	}
	return false, "", "Invalid Username or Password\n"

}

func checkaccount(username string, password string) bool {
	if username == "user1" && password == "test1"{
	return true}
	if username == "user2" && password == "test2"{
	return true}
	if username == "user3" && password == "test3"{
	return true}
	return false
}

func authenticating(client_conn net.Conn){
	AuthenticatedClient_conns[client_conn] = client_conn.RemoteAddr().String()
	//fmt.Println("1")
	sendto(client_conn, []byte("[authenticated]\n"))
	welcomemessage := fmt.Sprintf("A new client '%s' connected\n# of connected clients %d\n", client_conn.RemoteAddr().String(), len(AuthenticatedClient_conns))
	fmt.Println(welcomemessage)
	go sendtoAll([]byte (welcomemessage))
	go client_goroutine(client_conn)
	//fmt.Println("2")

}


	

